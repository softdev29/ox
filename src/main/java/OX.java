
import java.util.Scanner;

public class OX {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] bord = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        boolean check = false;
        int row, col;
        int count = 9;

        System.out.println("Welcome to OX Game");

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(bord[i][j] + " ");
            }
            System.out.println();
        }
        while (check == false) {

            System.out.println();
            System.out.println("Turn O");
            System.out.println("Please input row, col:");

            row = kb.nextInt();
            col = kb.nextInt();
            while (row > 2 || row < 0 || col > 2 || col < 0) {
                System.out.println("Please input number 0-2");
                System.out.println("Turn O");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            while (bord[row][col]!='-') {
                System.out.println("This posision has already been used. Pless try again.");
                System.out.println("Turn O");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            count--;

            bord[row][col] = 'O';
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(bord[i][j] + "  ");
                }
                System.out.println();
            }

            if (bord[0][0] == bord[0][1] && bord[0][1] == bord[0][2] && bord[0][0] != '-'
                    || bord[1][0] == bord[1][1] && bord[1][1] == bord[1][2] && bord[1][0] != '-'
                    || bord[2][0] == bord[2][1] && bord[2][1] == bord[2][2] && bord[2][0] != '-'
                    || bord[0][0] == bord[1][0] && bord[1][0] == bord[2][0] && bord[0][0] != '-'
                    || bord[0][1] == bord[1][1] && bord[1][1] == bord[2][1] && bord[0][1] != '-'
                    || bord[0][2] == bord[1][2] && bord[1][2] == bord[2][2] && bord[0][2] != '-'
                    || bord[0][0] == bord[1][1] && bord[1][1] == bord[2][2] && bord[0][0] != '-'
                    || bord[0][2] == bord[1][1] && bord[1][1] == bord[2][0] && bord[0][2] != '-') {
                check = true;
                System.out.println();
                System.out.println(">>>O Win<<<");
                break;
            }
            if (count == 0) {
                System.out.println();
                System.out.println(">>>Draw<<<");
                break;
            }
            System.out.println();
            System.out.println("Turn X");
            System.out.println("Please input row, col:");

            row = kb.nextInt();
            col = kb.nextInt();
            
            while (row > 2 || row < 0 || col > 2 || col < 0) {
                System.out.println("Please input number 0-2");
                System.out.println("Turn X");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            while (bord[row][col]!='-') {
                System.out.println("This posision has already been used. Pless try again.");
                System.out.println("Turn X");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            
            count--;

            bord[row][col] = 'X';
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(bord[i][j] + "  ");
                }
                System.out.println();
            }

            if (bord[0][0] == bord[0][1] && bord[0][1] == bord[0][2] && bord[0][0] != '-'
                    || bord[1][0] == bord[1][1] && bord[1][1] == bord[1][2] && bord[1][0] != '-'
                    || bord[2][0] == bord[2][1] && bord[2][1] == bord[2][2] && bord[2][0] != '-'
                    || bord[0][0] == bord[1][0] && bord[1][0] == bord[2][0] && bord[0][0] != '-'
                    || bord[0][1] == bord[1][1] && bord[1][1] == bord[2][1] && bord[0][1] != '-'
                    || bord[0][2] == bord[1][2] && bord[1][2] == bord[2][2] && bord[0][2] != '-'
                    || bord[0][0] == bord[1][1] && bord[1][1] == bord[2][2] && bord[0][0] != '-'
                    || bord[0][2] == bord[1][1] && bord[1][1] == bord[2][0] && bord[0][2] != '-') {
                check = true;
                System.out.println();
                System.out.println(">>>X Win<<<");
                break;
            }
            if (count == 0) {
                System.out.println();
                System.out.println(">>>Draw<<<");
                break;
            }

        }

    }
}
